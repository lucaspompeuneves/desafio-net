﻿namespace DesafioHackathon.Models
{
    public class Carta
    {
        public int Id { get; set; }
        public string Mensagem { get; set; }
        public string Destinatario { get; set; }
        public string Remetente { get; set; }
        public string Endereco { get; set; }
        public string Nome { get; set; }
        public string Contato { get; set; }



    }
}
