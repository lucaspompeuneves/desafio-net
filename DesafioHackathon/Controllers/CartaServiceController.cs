﻿using DesafioHackathon.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioHackathon.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartaServiceController : ControllerBase
    {
        CartaContext _cartaContext;

        public CartaServiceController(CartaContext cartaContext)
        {
            _cartaContext = cartaContext;
        }


        //GET: api/CartaService
        [HttpGet]
        [Route("ObterCarta")]
        public IEnumerable<Carta> Get()
        {
           return _cartaContext.Carta.ToList();
        }


        // GET: api/CartaService/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return string.Empty;
        }

        // POST: api/CartaService
        [HttpPost]
        [Route("Salvar")]
        public void Post(Carta carta)
        {
            _cartaContext.Add(carta);
            _cartaContext.SaveChanges();
        }

        // PUT: api/CartaService/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
